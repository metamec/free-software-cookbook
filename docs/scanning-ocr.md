# Scanning and OCR on GNU/Linux

## 1. Scanning

**SANE** (Scanner Access Now Easy) provides a library and a command-line tool to use scanners under GNU/Linux.

The two main GUI frontends are:

* **Simple Scan**
(_a simplified GUI, as it's name implies_)
* **XSane**
(_A full-featured GTK-based frontend._)

### XSane

![img "The document before"](images/scanning-ocr/xsane-viewer.jpg)

Define the settings and select Scan.

Resolution:
![img "The document before"](images/scanning-ocr/xsane-resolution.jpg)  
Select 300 dpi if you want to use OCR.

Color:
![img "The document before"](images/scanning-ocr/xsane-colormode.jpg)  
Select Gray if you want to use OCR.

For more details on the **Viewer**, **Scan** options and available formats: <http://www.xsane.org/doc/sane-xsane-viewer-doc.html>  
<http://www.xsane.org/doc/sane-xsane-save-doc.html>

You can also acquire a scan **Preview**:  
<http://www.xsane.org/doc/sane-xsane-preview-doc.html>

### Scan using GIMP

GIMP can import an image through XSane.

> File > Create > Xsane: Device dialog...

![GIMP menu to import image from XSane](images/scanning-ocr/gimp-menu.png)

You perform the scan in XSane (Select the options and press Scan).

The image opens and is directly editable through GIMP.



### Improve scanned document in GIMP 

**Manually**

One simple method is to use Levels. Go to menu:

> Colors > Levels...

Play with the three markers (black, gray and white) until you achieve a satisfactory result.
(Note: Going too far may render the letters illegible)
You can add the settings to Presets to reuse it easily in the feature (Press the "Plus" sign on the right of Presets).

Example:

Before

![img "The document before"](images/scanning-ocr/before.png)

*Note: I had to desaturate the image to make it grayscale (Colors > Desaturate).*

![img "The document with desaturation"](images/scanning-ocr/desaturate.png)


After

![img "The document after"](images/scanning-ocr/after.png)



**Automation**

You can automate the process with a Plugin called [Leon](http://registry.gimp.org/node/24971) to *"Clear paper to "white" for a scanned pictures"*

Learn [How to install Script-Fu plugins in GIMP](http://docs.gimp.org/en/install-script-fu.html)

After installing the .scm file in the Scripts directory, go to GIMP menu:

> Filters > Script-FU > Refresh scripts

Then, it should appear in

> Filters > Leon > Reset background to white

![img "The document with Leon"](images/scanning-ocr/leon.png)

**Drawings**

A more elaborate method, particularly helpful for drawings is described here:

*Tutorial! How to Clear up Scan a [Drawing] with Gimp* [(video)](https://www.youtube.com/watch?v=1_vEI8Jqtlo)

If you have other better methods, please share!

## 2. OCR

There are several GUI programs for performing OCR on GNU/Linux.
To name a few, you can try: ocrfeeder, yagf

The main available open source OCR engines are Tesseract and Cuneiform. 

Here we are focussing on **Tesseract** in conjunction with XSane, through a command line program called xsane2tess.

### OCR through XSane

Make sure you have installed the following packages:

* xsane  
* tesseract-ocr (or tesseract)  
* tesseract data per language:  
*greek:* tesseract-ocr-ell (or tesseract-data-ell)  
*anciant greek:* tesseract-ocr-grc (or tesseract-data-grc)  
*english:* tesseract-ocr-eng (or tesseract-data-eng)  
* xsane2tess

In XSane, go to

> Preferences > Setup

and enter following (change the language as per the language of the document.  
If the document is mixed, you can try adding muliple languages, with a comma)

![img "The document before"](images/scanning-ocr/ocr-on-xsane.png)

After clicking Apply/OK, select:

Type: TEXT  
Resolution:
![img](images/scanning-ocr/xsane-resolution.jpg) 300  
Color: ![img](images/scanning-ocr/xsane-colormode.jpg) Gray

You then just have to Scan (the .txt file will be automatically saved).

Alternatively, you can select Type: JPEG etc. (an image file format) and use the View window to make OCR on a specific area.

OCR button on Viewer: ![img](images/scanning-ocr/xsane-ocr.jpg)

More details on the Viewer: <http://www.xsane.org/doc/sane-xsane-viewer-doc.html>

### Fixing scanned OCR text with LibreOffice

to do